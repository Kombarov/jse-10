package ru.kombarov.tm;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.context.Bootstrap;

public final class Application {

    public static void main(String[] args) throws Exception {

        final @NotNull Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }
}
