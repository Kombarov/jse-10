package ru.kombarov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.enumerated.Role;

import javax.xml.bind.annotation.XmlInlineBinaryData;
import javax.xml.bind.annotation.XmlTransient;

@Getter
@Setter
@NoArgsConstructor
public final class User extends AbstractEntity {

    @Nullable
    private String login;

    @Nullable
    private String password;

    @Nullable
    private Role role;

    public User(final @NotNull Role role) {
        this.role = role;
    }
}
