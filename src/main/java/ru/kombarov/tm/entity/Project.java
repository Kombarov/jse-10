package ru.kombarov.tm.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.enumerated.Status;

import java.util.Date;

import static ru.kombarov.tm.util.DateUtil.parseDateToString;

@NoArgsConstructor
public final class Project extends AbstractEntity {

    @Getter
    @Setter
    @Nullable
    private String name;

    @Getter
    @Setter
    @Nullable
    private String userId;

    @Getter
    @Setter
    @Nullable
    private String description;

    @Setter
    @Nullable
    @JsonFormat(pattern = "dd.MM.yyyy")
    private Date dateStart;

    @Setter
    @Nullable
    @JsonFormat(pattern="dd.MM.yyyy")
    private Date dateFinish;

    @Getter
    @Setter
    @NotNull
    private Status status;

    public Project(final @Nullable String name) {
        this.name = name;
        this.status = Status.PLANNED;
    }

    @Nullable
    public String getDateStart() {
        if (dateStart == null) return null;
        return parseDateToString(dateStart);
    }

    @Nullable
    public String getDateFinish() {
        if (dateFinish == null) return null;
        return parseDateToString(dateFinish);
    }
}
