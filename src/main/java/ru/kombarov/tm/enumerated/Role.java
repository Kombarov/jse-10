package ru.kombarov.tm.enumerated;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

@Getter
@AllArgsConstructor
public enum Role {

    ADMINISTRATOR("Administrator"),
    USER("User");

    @NotNull
    private final String roleName;
}
