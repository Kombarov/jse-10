package ru.kombarov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.IRepository;
import ru.kombarov.tm.entity.Project;
import ru.kombarov.tm.entity.Task;
import ru.kombarov.tm.enumerated.Status;
import ru.kombarov.tm.util.comparator.task.TaskDateFinishComparator;
import ru.kombarov.tm.util.comparator.task.TaskDateStartComparator;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements IRepository<Task> {

    @NotNull
    @Override
    public List<Task> findAll(final @NotNull String userId) throws Exception {
        final @NotNull List<Task> list = new ArrayList<Task>();
        for (final @NotNull Task task : findAll()) {
            if (task.getUserId() == null || task.getUserId().isEmpty()) throw new Exception();
            if (task.getUserId().equals(userId)) list.add(task);
        }
        return list;
    }

    @Nullable
    @Override
    public Task findOne(final @NotNull String userId, final @NotNull String id) throws Exception {
        for (final @NotNull Task task : findAll(userId)) {
            if (task.getId() == null || task.getId().isEmpty()) throw new Exception();
            if (task.getId().equals(id)) return task;
        }
        return null;
    }

    @Override
    public void removeAll(final @NotNull String userId) throws Exception {
        for (final @NotNull Task task : findAll()) {
            if (task.getUserId() == null || task.getUserId().isEmpty()) throw new Exception();
            if (task.getId() == null || task.getId().isEmpty()) throw new Exception();
            if (task.getUserId().equals(userId)) remove(task.getId());
        }
    }

    @Nullable
    @Override
    public String getIdByName(final @NotNull String name) throws Exception {
        final @NotNull List<Task> tasks = findAll();
        for (final @NotNull Task task : tasks) {
            if (task.getName() == null || task.getName().isEmpty()) throw new Exception();
            if (task.getName().equals(name)) return task.getId();
        }
        return null;
    }

    @NotNull
    public List<Task> getTasksByProjectId(final @NotNull String projectId) throws Exception {
        final @NotNull List<Task> list = findAll();
        final @NotNull List<Task> sortedList = new ArrayList<>();
        for (final @NotNull Task task : list) {
            if (task.getProjectId() == null || task.getProjectId().isEmpty()) throw new Exception();
            if (task.getProjectId().equals(projectId)) sortedList.add(task);
        }
        return sortedList;
    }

    @NotNull
    public List<Task> getTasksByUserId(final @NotNull String userId) throws Exception {
        final @NotNull List<Task> list = findAll();
        final @NotNull List<Task> sortedList = new ArrayList<>();
        for (final @NotNull Task task : list) {
            if (task.getUserId() == null || task.getUserId().isEmpty()) throw new Exception();
            if (task.getUserId().equals(userId)) sortedList.add(task);
        }
        return sortedList;
    }

    @NotNull
    public List<Task> sortByDateStart(final @NotNull String userId) throws Exception {
        final @NotNull List<Task> list = findAll(userId);
        final @NotNull Comparator<Task> comparator = new TaskDateStartComparator();
        list.sort(comparator);
        return list;
    }

    @NotNull
    public List<Task> sortByDateFinish(final @NotNull String userId) throws Exception {
        final @NotNull List<Task> list = findAll(userId);
        final @NotNull Comparator<Task> comparator = new TaskDateFinishComparator();
        list.sort(comparator);
        return list;
    }

    @NotNull
    public List<Task> sortByStatus(final @NotNull Status status, final @NotNull String userId) throws Exception {
        final @NotNull List<Task> list = findAll(userId);
        final @NotNull List<Task> sortedList = new ArrayList<>();
        for (final @NotNull Task task : list) {
            if (task.getStatus().equals(status)) sortedList.add(task);
        }
        return sortedList;
    }

    @NotNull
    public List<Task> findByPart(final @NotNull String part, final @NotNull String userId) throws Exception {
        final @NotNull List<Task> list = findAll(userId);
        final @NotNull List<Task> sortedList = new ArrayList<>();
        for (final @NotNull Task task : list) {
            if (task.getName() == null || task.getName().isEmpty()) throw new Exception();
            if (task.getName().contains(part)) sortedList.add(task);
            if (task.getDescription() == null || task.getDescription().isEmpty()) throw new Exception();
            if (task.getDescription().contains(part)) sortedList.add(task);
        }
        return sortedList;
    }
}
