package ru.kombarov.tm.command.data.load;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.command.AbstractCommand;
import ru.kombarov.tm.constant.DataConstant;
import ru.kombarov.tm.datatransfer.DataTransfer;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;

public class DataBinLoadCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "data bin load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from binary format.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BIN LOAD]");
        @NotNull
        final File file = new File(DataConstant.FILE_BINARY);
        @NotNull
        final FileInputStream fileInputStream = new FileInputStream(file);
        @NotNull
        final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull
        final DataTransfer dataTransfer = (DataTransfer) objectInputStream.readObject();
        objectInputStream.close();
        fileInputStream.close();
        if (serviceLocator == null) return;
        serviceLocator.getUserService().persist(dataTransfer.getUsers());
        serviceLocator.getProjectService().persist(dataTransfer.getProjects());
        serviceLocator.getTaskService().persist(dataTransfer.getTasks());
        System.out.println("[OK]");
    }
}
