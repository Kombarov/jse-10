package ru.kombarov.tm.command.data.save;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.command.AbstractCommand;
import ru.kombarov.tm.constant.DataConstant;
import ru.kombarov.tm.datatransfer.DataTransfer;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.nio.file.Files;

public class DataJaxBXmlSaveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "data JaxB xml save";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data by JaxB to XML format.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JAXB XML SAVE]");
        @NotNull
        final DataTransfer dataTransfer = new DataTransfer();
        if (serviceLocator == null) return;
        dataTransfer.load(serviceLocator);
        @NotNull
        final File file = new File(DataConstant.FILE_XML);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        @NotNull
        final JAXBContext jaxbContext = JAXBContext.newInstance(DataTransfer.class);
        @NotNull
        final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(dataTransfer, file);
        System.out.println("[OK]");
    }
}
