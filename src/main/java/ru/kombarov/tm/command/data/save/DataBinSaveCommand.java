package ru.kombarov.tm.command.data.save;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.command.AbstractCommand;
import ru.kombarov.tm.constant.DataConstant;
import ru.kombarov.tm.datatransfer.DataTransfer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;

public class DataBinSaveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "data bin save";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to binary format.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BIN SAVE]");
        @NotNull
        final DataTransfer dataTransfer = new DataTransfer();
        if (serviceLocator == null) return;
        dataTransfer.load(serviceLocator);
        @NotNull
        final File file = new File(DataConstant.FILE_BINARY);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        @NotNull
        final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull
        final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(dataTransfer);
        objectOutputStream.close();
        fileOutputStream.close();
        System.out.println("[OK]");
    }
}
