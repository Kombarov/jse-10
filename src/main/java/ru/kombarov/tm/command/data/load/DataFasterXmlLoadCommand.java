package ru.kombarov.tm.command.data.load;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.command.AbstractCommand;
import ru.kombarov.tm.constant.DataConstant;
import ru.kombarov.tm.datatransfer.DataTransfer;

import java.io.File;

public class DataFasterXmlLoadCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "data fasterXml load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load data by FasterXML from XML format.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA FASTERXML LOAD]");
        @NotNull
        final File file = new File(DataConstant.FILE_XML);
        @NotNull
        final XmlMapper xmlMapper = new XmlMapper();
        @NotNull
        final DataTransfer dataTransfer = xmlMapper.readValue(file, DataTransfer.class);
        if (serviceLocator == null) return;
        serviceLocator.getUserService().persist(dataTransfer.getUsers());
        serviceLocator.getProjectService().persist(dataTransfer.getProjects());
        serviceLocator.getTaskService().persist(dataTransfer.getTasks());
        System.out.println("[OK]");
    }
}
