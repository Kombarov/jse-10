package ru.kombarov.tm.command.data.save;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.command.AbstractCommand;
import ru.kombarov.tm.constant.DataConstant;
import ru.kombarov.tm.datatransfer.DataTransfer;

import java.io.File;
import java.nio.file.Files;

public class DataFasterXmlSaveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "data fasterXml save";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data by FasterXML to XML format.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA FASTERXML SAVE]");
        @NotNull
        final DataTransfer dataTransfer = new DataTransfer();
        if (serviceLocator == null) return;
        dataTransfer.load(serviceLocator);
        @NotNull
        final File file = new File(DataConstant.FILE_XML);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        @NotNull
        final XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.writerWithDefaultPrettyPrinter().writeValue(file, dataTransfer);
        System.out.println("[OK]");
    }
}
