package ru.kombarov.tm.command.data.load;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.command.AbstractCommand;
import ru.kombarov.tm.constant.DataConstant;
import ru.kombarov.tm.datatransfer.DataTransfer;

import java.io.File;

public class DataJaxBJSONLoadCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "data JaxB json load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load data by JaxB from JSON format.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JAXB JSON LOAD]");
        @NotNull
        final File file = new File(DataConstant.FILE_JSON);
        @NotNull
        final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull
        final DataTransfer dataTransfer = objectMapper.readValue(file, DataTransfer.class);
        if (serviceLocator == null) return;
        serviceLocator.getUserService().persist(dataTransfer.getUsers());
        serviceLocator.getProjectService().persist(dataTransfer.getProjects());
        serviceLocator.getTaskService().persist(dataTransfer.getTasks());
        System.out.println("[OK]");
    }
}
