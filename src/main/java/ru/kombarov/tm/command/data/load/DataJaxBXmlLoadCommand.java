package ru.kombarov.tm.command.data.load;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.command.AbstractCommand;
import ru.kombarov.tm.constant.DataConstant;
import ru.kombarov.tm.datatransfer.DataTransfer;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class DataJaxBXmlLoadCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "data JaxB xml load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load data by JaxB from XML format.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JAXB XML LOAD]");
        @NotNull
        final File file = new File(DataConstant.FILE_XML);
        @NotNull
        final JAXBContext jaxbContext = JAXBContext.newInstance(DataTransfer.class);
        @NotNull
        final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @NotNull
        final DataTransfer dataTransfer = (DataTransfer) unmarshaller.unmarshal(file);
        if (serviceLocator == null) return;
        serviceLocator.getUserService().persist(dataTransfer.getUsers());
        serviceLocator.getProjectService().persist(dataTransfer.getProjects());
        serviceLocator.getTaskService().persist(dataTransfer.getTasks());
        System.out.println("[OK]");
    }
}
