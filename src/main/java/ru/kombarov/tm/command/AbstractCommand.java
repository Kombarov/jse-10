package ru.kombarov.tm.command;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.ServiceLocator;
import ru.kombarov.tm.context.Bootstrap;

import java.io.BufferedReader;
import java.io.InputStreamReader;

@Setter
public abstract class AbstractCommand {

    @Nullable
    protected ServiceLocator serviceLocator;

    @Nullable
    protected Bootstrap bootstrap;

    @NotNull
    protected final BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    @NotNull
    public abstract String command();

    @NotNull
    public abstract String description();

    public abstract void execute() throws Exception;
}
