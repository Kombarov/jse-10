package ru.kombarov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.command.AbstractCommand;

import static ru.kombarov.tm.util.EntityUtil.*;

public final class UserViewCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "user-view";
    }

    @NotNull
    @Override
    public String description() {
        return "Show user info.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER VIEW]");
        if (serviceLocator == null) throw new Exception();
        printUser(serviceLocator.getUserService().getUserCurrent());
        printProjects(serviceLocator.getProjectService().getProjectsByUserId(serviceLocator.getUserService().getUserCurrent().getId()));
        System.out.println();
        System.out.println("tasks:");
        printTasks(serviceLocator.getTaskService().getTasksByUserId(serviceLocator.getUserService().getUserCurrent().getId()));
        System.out.println("[OK]");
    }
}
