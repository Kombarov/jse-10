package ru.kombarov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.command.AbstractCommand;
import ru.kombarov.tm.entity.User;

import static ru.kombarov.tm.util.PasswordUtil.generateHash;

public final class UserChangePasswordCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "user-change password";
    }

    @NotNull
    @Override
    public String description() {
        return "Change user password.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD");
        final @Nullable String password = input.readLine();
        @Nullable User user = new User();
        if (serviceLocator == null) throw new Exception();
        user = serviceLocator.getUserService().getUserCurrent();
        if (user != null) user.setPassword(generateHash(password));
        serviceLocator.getUserService().setUserCurrent(user);
        serviceLocator.getUserService().merge(user);
        System.out.println("[OK]");
    }
}
