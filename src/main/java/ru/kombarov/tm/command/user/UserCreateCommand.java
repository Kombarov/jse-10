package ru.kombarov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.command.AbstractCommand;
import ru.kombarov.tm.entity.User;
import ru.kombarov.tm.enumerated.Role;

import static ru.kombarov.tm.util.PasswordUtil.generateHash;

public final class UserCreateCommand extends AbstractCommand {

    @Override
    public @NotNull String command() {
        return "user-create";
    }

    @Override
    public @NotNull String description() {
        return "Create new User.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER CREATE]");
        System.out.println("ENTER USERNAME");
        if (serviceLocator == null) throw new Exception();
        @Nullable String name = input.readLine();
        for (final @NotNull User user : serviceLocator.getUserService().findAll()) {
            if (user.getLogin() == null || user.getLogin().isEmpty()) return;
            while (user.getLogin().equals(name)) {
                System.out.println("THIS LOGIN EXISTS");
                System.out.println("ENTER ANOTHER NAME");
                name = input.readLine();
            }
        }
        System.out.println("TYPE YOUR ROLE: USER OR ADMINISTRATOR");
        final @NotNull String role = input.readLine();
        final @NotNull User user = new User(Role.valueOf(role.toUpperCase()));
        user.setLogin(name);
        System.out.println("ENTER PASSWORD");
        final @Nullable String hash = generateHash(input.readLine());
        user.setPassword(hash);
        serviceLocator.getUserService().persist(user);
        System.out.println("[OK]");
    }
}
