package ru.kombarov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.command.AbstractCommand;

public final class ProjectClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all projects.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT CLEAR]");
        if (serviceLocator == null) throw new Exception();
        if (serviceLocator.getUserService().getUserCurrent() == null) throw new Exception();
        serviceLocator.getProjectService().removeAll(serviceLocator.getUserService().getUserCurrent().getId());
        System.out.println("[OK]");
    }
}
