package ru.kombarov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.command.AbstractCommand;

import static ru.kombarov.tm.util.EntityUtil.printProjects;

public class ProjectListByDateStartCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "project-list show by start date";
    }

    @NotNull
    @Override
    public String description() {
        return "Show projects by start date.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT LIST BY START DATE]");
        if (serviceLocator == null) throw new Exception();
        if (serviceLocator.getUserService().getUserCurrent() == null) throw new Exception();
        printProjects(serviceLocator.getProjectService().sortByDateStart(serviceLocator.getUserService().getUserCurrent().getId()));
        System.out.println("[OK]");
    }
}
