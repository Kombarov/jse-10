package ru.kombarov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.command.AbstractCommand;
import ru.kombarov.tm.entity.Project;

import static ru.kombarov.tm.util.DateUtil.parseStringToDate;

public final class ProjectCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "project-create";
    }

    @NotNull
    @Override
    public String description() {
        return "Create new project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER PROJECT NAME");
        final @NotNull Project project = new Project(input.readLine());
        System.out.println("ENTER PROJECT DESCRIPTION");
        project.setDescription(input.readLine());
        System.out.println("ENTER START DATE");
        project.setDateStart(parseStringToDate(input.readLine()));
        System.out.println("ENTER FINISH DATE");
        project.setDateFinish(parseStringToDate(input.readLine()));
        if (serviceLocator == null) throw new Exception();
        if (serviceLocator.getUserService().getUserCurrent() == null) throw new Exception();
        project.setUserId(serviceLocator.getUserService().getUserCurrent().getId());
        serviceLocator.getProjectService().persist(project);
        System.out.println("[OK]");
    }
}
