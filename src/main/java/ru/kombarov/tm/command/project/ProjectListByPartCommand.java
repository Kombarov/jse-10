package ru.kombarov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.command.AbstractCommand;
import ru.kombarov.tm.util.EntityUtil;

public class ProjectListByPartCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "project-list show by part";
    }

    @NotNull
    @Override
    public String description() {
        return "Show projects by part of the name/description.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT LIST BY PART]");
        System.out.println("ENTER PART OF THE NAME OR DESCRIPTION");
        final @Nullable String part = input.readLine();
        if (serviceLocator == null) throw new Exception();
        if (serviceLocator.getUserService().getUserCurrent() == null) throw new Exception();
        EntityUtil.printProjects(serviceLocator.getProjectService().findByPart(part, serviceLocator.getUserService().getUserCurrent().getId()));
        System.out.println("[OK]");
    }
}
