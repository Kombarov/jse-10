package ru.kombarov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.command.AbstractCommand;
import ru.kombarov.tm.enumerated.Status;
import ru.kombarov.tm.util.EntityUtil;

public class ProjectListByStatusCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "project-list show by status";
    }

    @NotNull
    @Override
    public String description() {
        return "Show projects by status.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT LIST BY STATUS]");
        System.out.println(Status.PLANNED.displayName() + ":");
        if (serviceLocator == null) throw new Exception();
        if (serviceLocator.getUserService().getUserCurrent() == null) throw new Exception();
        EntityUtil.printProjects(serviceLocator.getProjectService().sortByStatus(Status.PLANNED, serviceLocator.getUserService().getUserCurrent().getId()));
        System.out.println("\n" + Status.DONE.displayName() + ":");
        EntityUtil.printProjects(serviceLocator.getProjectService().sortByStatus(Status.DONE, serviceLocator.getUserService().getUserCurrent().getId()));
        System.out.println("\n" + Status.IN_PROCESS.displayName() + ":");
        EntityUtil.printProjects(serviceLocator.getProjectService().sortByStatus(Status.IN_PROCESS, serviceLocator.getUserService().getUserCurrent().getId()));
        System.out.println("[OK]");
    }
}
