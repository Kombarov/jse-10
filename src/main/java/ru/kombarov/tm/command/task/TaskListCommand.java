package ru.kombarov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.command.AbstractCommand;

import static ru.kombarov.tm.util.EntityUtil.printTasks;

public final class TaskListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show all tasks.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK LIST]");
        if (serviceLocator == null) throw new Exception();
        if (serviceLocator.getUserService().getUserCurrent() == null) throw new Exception();
        printTasks(serviceLocator.getTaskService().findAll(serviceLocator.getUserService().getUserCurrent().getId()));
        System.out.println("[OK]");
    }
}
