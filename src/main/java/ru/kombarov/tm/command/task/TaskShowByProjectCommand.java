package ru.kombarov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.command.AbstractCommand;

import static ru.kombarov.tm.util.EntityUtil.printProjects;
import static ru.kombarov.tm.util.EntityUtil.printTasks;

public final class TaskShowByProjectCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-show by project";
    }

    @NotNull
    @Override
    public String description() {
        return "Show tasks by project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK SHOW BY PROJECT]");
        if (serviceLocator == null) throw new Exception();
        if (serviceLocator.getUserService().getUserCurrent() == null) throw new Exception();
        printProjects(serviceLocator.getProjectService().findAll(serviceLocator.getUserService().getUserCurrent().getId()));
        System.out.println("ENTER PROJECT NAME");
        printTasks(serviceLocator.getTaskService().getTasksByProjectId(serviceLocator.getProjectService().getIdByName(input.readLine())));
        System.out.println("[OK]");
    }
}
