package ru.kombarov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.command.AbstractCommand;
import ru.kombarov.tm.entity.Task;

import static ru.kombarov.tm.util.EntityUtil.printProjects;
import static ru.kombarov.tm.util.EntityUtil.printTasks;

public final class TaskAttachCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-attach";
    }

    @NotNull
    @Override
    public String description() {
        return "Attach task to project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK ATTACH]");
        if (serviceLocator == null) throw new Exception();
        if (serviceLocator.getUserService().getUserCurrent() == null) throw new Exception();
        printTasks(serviceLocator.getTaskService().findAll(serviceLocator.getUserService().getUserCurrent().getId()));
        System.out.println("ENTER TASK NAME");
        final @NotNull String taskName = input.readLine();
        printProjects(serviceLocator.getProjectService().findAll(serviceLocator.getUserService().getUserCurrent().getId()));
        System.out.println("ENTER PROJECT NAME TO ATTACH TASK");
        final @Nullable String projectName = input.readLine();
        final @Nullable Task task = serviceLocator.getTaskService().findOne(serviceLocator.getTaskService().getIdByName(taskName));
        if (task == null) throw new Exception();
        task.setProjectId(serviceLocator.getProjectService().getIdByName(projectName));
        System.out.println("[OK]");
    }
}
