package ru.kombarov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.command.AbstractCommand;

import static ru.kombarov.tm.util.EntityUtil.printTasks;

public class TaskListByDateStartCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-list show by start date";
    }

    @NotNull
    @Override
    public String description() {
        return "Show tasks by start date.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK LIST BY START DATE]");
        if (serviceLocator == null) throw new Exception();
        if (serviceLocator.getUserService().getUserCurrent() == null) throw new Exception();
        printTasks(serviceLocator.getTaskService().sortByDateStart(serviceLocator.getUserService().getUserCurrent().getId()));
        System.out.println("[OK]");
    }
}
