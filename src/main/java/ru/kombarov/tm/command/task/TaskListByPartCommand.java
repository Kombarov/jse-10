package ru.kombarov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.command.AbstractCommand;
import ru.kombarov.tm.util.EntityUtil;

public class TaskListByPartCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-list show by part";
    }

    @NotNull
    @Override
    public String description() {
        return "Show tasks by part of the name/description.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK LIST BY PART]");
        System.out.println("ENTER PART OF THE NAME OR DESCRIPTION");
        final @Nullable String part = input.readLine();
        if (serviceLocator == null) throw new Exception();
        if (serviceLocator.getUserService().getUserCurrent() == null) throw new Exception();
        EntityUtil.printTasks(serviceLocator.getTaskService().findByPart(part, serviceLocator.getUserService().getUserCurrent().getId()));
        System.out.println("[OK]");
    }
}
