package ru.kombarov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.command.AbstractCommand;

public final class HelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "help";
    }

    @NotNull
    @Override
    public String description() {
        return "Show all commands.";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap == null) throw new Exception();
        for (final @NotNull AbstractCommand command : bootstrap.getCommands()) {
            System.out.println(command.command() + ": " + command.description());
        }
    }
}
