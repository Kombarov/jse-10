package ru.kombarov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.IService;
import ru.kombarov.tm.entity.Project;
import ru.kombarov.tm.enumerated.Status;
import ru.kombarov.tm.repository.AbstractRepository;
import ru.kombarov.tm.repository.ProjectRepository;

import java.util.List;

public final class ProjectService extends AbstractService<Project> implements IService<Project> {

    @NotNull
    private final ProjectRepository projectRepository = (ProjectRepository) abstractRepository;

    public ProjectService(AbstractRepository<Project> abstractRepository) {
        super(abstractRepository);
    }

    @Nullable
    @Override
    public String getIdByName(final @Nullable String name) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        else return abstractRepository.getIdByName(name);
    }

    @NotNull
    public List<Project> getProjectsByUserId(final @Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        return projectRepository.getProjectsByUserId(userId);
    }

    @NotNull
    @Override
    public List<Project> findAll(final @Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        return abstractRepository.findAll(userId);
    }

    @Nullable
    @Override
    public Project findOne(final @Nullable String userId, final @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        return abstractRepository.findOne(userId, id);
    }

    @Override
    public void removeAll(final @Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        abstractRepository.removeAll(userId);
    }

    @NotNull
    public List<Project> sortByDateStart(final @Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        return projectRepository.sortByDateStart(userId);
    }

    @NotNull
    public List<Project> sortByDateFinish(final @Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        return projectRepository.sortByDateFinish(userId);
    }

    @NotNull
    public List<Project> sortByStatus(final @NotNull Status status, final @Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        return projectRepository.sortByStatus(status, userId);
    }

    @NotNull
    public List<Project> findByPart(final @Nullable String part, final @Nullable String userId) throws Exception {
        if (part == null || part.isEmpty()) throw new Exception();
        if (userId == null || userId.isEmpty()) throw new Exception();
        return projectRepository.findByPart(part, userId);
    }
}