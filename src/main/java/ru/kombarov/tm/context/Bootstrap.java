package ru.kombarov.tm.context;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.kombarov.tm.api.ServiceLocator;
import ru.kombarov.tm.command.*;
import ru.kombarov.tm.repository.ProjectRepository;
import ru.kombarov.tm.repository.TaskRepository;
import ru.kombarov.tm.repository.UserRepository;
import ru.kombarov.tm.service.ProjectService;
import ru.kombarov.tm.service.TaskService;
import ru.kombarov.tm.service.UserService;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;

public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final ProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final TaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final UserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final ProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final TaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final UserService userService = new UserService(userRepository);

    @NotNull
    private final BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public void init() throws Exception {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        final @NotNull Reflections reflections = new Reflections("ru.kombarov.tm.command");
        final @NotNull Set<Class<? extends AbstractCommand>> classes = reflections.getSubTypesOf(ru.kombarov.tm.command.AbstractCommand.class);
        for (final @NotNull Class<? extends AbstractCommand> clazz : classes) {
            registry(clazz.newInstance());
        }
        @NotNull String command = "";
        while (!"exit".equals(command)) {
            command = input.readLine();
            try {
                execute(command);
            }
            catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private void execute(final @Nullable String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        final @Nullable AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;
        final @NotNull List<String> commandList = new ArrayList<>();
        commandList.add("about");
        commandList.add("help");
        commandList.add("user-create");
        commandList.add("user-login");
        if (commandList.contains(abstractCommand.command())) abstractCommand.execute();
        else if (userService.getUserCurrent() != null) abstractCommand.execute();
    }

    public void registry(final @NotNull AbstractCommand command) throws Exception {
        final @NotNull String cliCommand = command.command();
        final @NotNull String cliDescription = command.description();
        if (cliCommand.isEmpty()) throw new Exception();
        if (cliDescription.isEmpty()) throw new Exception();
        command.setServiceLocator(this);
        command.setBootstrap(this);
        commands.put(cliCommand, command);
    }

    @NotNull
    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }
}